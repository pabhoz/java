/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package holamundojava;

import java.util.Scanner;

/**
 *
 * @author PabloAnibal
 */
public class HolaMundoJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //System.out.printf("El resultado es: %d",restaPorConsola());
        /*Scanner entrada = new Scanner(System.in);
        float n1;
        float n2;
        
        System.out.print("Escriba el primer número: ");
        n1 = entrada.nextFloat();
        
        System.out.print("Escriba el segundo número: ");
        n2 = entrada.nextFloat();
        
        resta(n1,n2);
        suma(n1,n2);
        multiplicacion(n1,n2);
        division(n1,n2);
        
        par((int)n1);//Envoltorio (Tipo)variable
        */
        System.out.print(primo());
        
    }
    public static void resta(float a, float b){
        
        System.out.printf(" %f - %f = %f\n", a,b,(a-b));
        
    }
    public static void suma(float a, float b){
        
        System.out.printf(" %f + %f = %f\n", a,b,(a+b));
        
    }
    public static void multiplicacion(float a, float b){
        
        System.out.printf(" %f * %f = %f\n", a,b,(a*b));
        
    }
    public static void division(float a, float b){
        
        System.out.printf(" %f / %f = %f\n", a,b,(a/b));
        
    }
    
    public static void par(int n){
        if(n%2 == 0){
            System.out.printf("El número %d es par",n);
        }else{
            System.out.printf("El número %d es NO es par",n);
        }
    }
    public static int restaPorConsola(){
        
        //Crear un objeto scanner que recibe entradas por ventana
        Scanner entrada = new Scanner( System.in );
        
        int n1;
        int n2;
        int resta;
        
        System.out.print("Escriba el primer número: ");
        n1 = entrada.nextInt();
        
        System.out.print("Escriba el segundo número: ");
        n2 = entrada.nextInt();
        
        resta = n1 - n2;
        
        return resta;
        
    }
    public static boolean primo(){
       
       boolean primo = false;
       boolean divisible = false;
       int divisor = 2;
       
       Scanner escaner = new Scanner( System.in );
       System.out.print("Ingrese el número a evaluar:");
       int n = escaner.nextInt();
       
       while( !divisible && divisor <= n){
           if(n%divisor == 0){
               divisible = true;
           }
       }
       
       primo = divisor == n;
       
       return primo;
    }
}


